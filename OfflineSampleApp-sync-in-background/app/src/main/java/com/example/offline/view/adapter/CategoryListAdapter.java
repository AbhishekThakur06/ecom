package com.example.offline.view.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.offline.R;
import com.example.offline.model.entity.CategoryInfo;
import com.example.offline.view.callback.CategoryClickCallback;

import java.util.List;

import timber.log.Timber;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {

    private final List<CategoryInfo> categoryInfoDataSet;
    private final CategoryClickCallback categoryClickCallback;
    public CategoryListAdapter(List<CategoryInfo> categoryInfoDataSet) {
        this.categoryInfoDataSet = categoryInfoDataSet;
        this.categoryClickCallback = null;
    }
    public CategoryListAdapter(List<CategoryInfo> categoryInfoDataSet, CategoryClickCallback categoryClickCallback) {
        this.categoryInfoDataSet = categoryInfoDataSet;
        this.categoryClickCallback = categoryClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TextView commentText = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_row, parent, false);
        return new ViewHolder(commentText);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CategoryInfo categoryInfo = categoryInfoDataSet.get(position);
        /*if (comment.isSyncPending()) {
            holder.commentText.setTextColor(Color.LTGRAY);
        } else {
        */    holder.commentText.setTextColor(Color.BLACK);
        //}
        holder.commentText.setText(categoryInfo.getCategoryName());
        holder.commentText.setOnClickListener(new OnCategoryClickListener(categoryInfoDataSet.get(position)));
    }

    @Override
    public int getItemCount() {
        return categoryInfoDataSet == null ? 0 : categoryInfoDataSet.size();
    }

    public void updateCommentList(List<CategoryInfo> categoryInfoList) {
        Timber.d("Got new categoryInfoDataSet " + categoryInfoList.size());
        this.categoryInfoDataSet.clear();
        this.categoryInfoDataSet.addAll(categoryInfoList);
        notifyDataSetChanged();
    }

    /**
     * View holder for shopping list items of this adapter
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView commentText;

        public ViewHolder(final TextView commentText) {
            super(commentText);
            this.commentText = commentText;
        }
    }

    class OnCategoryClickListener implements View.OnClickListener {
        CategoryInfo categoryInfo;
        OnCategoryClickListener(CategoryInfo categoryInfo) {
            this.categoryInfo = categoryInfo;
        }
        @Override
        public void onClick(View view) {
            categoryClickCallback.onClick(categoryInfo);
        }
    };
}
