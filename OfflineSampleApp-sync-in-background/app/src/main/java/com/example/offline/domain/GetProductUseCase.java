package com.example.offline.domain;

import com.example.offline.model.entity.ProductInfo;

import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * Created by Abhishek on 12/3/2017.
 */

public class GetProductUseCase {

    private final CategoryRepository categoryRepository;

    public GetProductUseCase(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public void getProducts(Consumer<List<ProductInfo>> consumer, int categoryId) {
        categoryRepository.products(consumer, categoryId);
    }

    public void getProductById(Consumer<ProductInfo> consumer,int categoryId, int productId) {
        categoryRepository.product(consumer, categoryId, productId);
    }
}
