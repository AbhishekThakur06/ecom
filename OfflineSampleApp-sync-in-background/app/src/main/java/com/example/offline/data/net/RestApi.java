
package com.example.offline.data.net;

import com.example.offline.model.ecommodel.EcomData;

import io.reactivex.Observable;

/**
 * RestApi for retrieving data from the network.
 */
public interface RestApi {

  /** Api url for getting all the products and their category */
  String API_URL_GET_CATEGORY_LIST = "https://stark-spire-93433.herokuapp.com/json";

  Observable<EcomData> ecomData();
}
