package com.example.offline.domain;

import com.example.offline.model.entity.ProductVariant;

import java.util.List;

import io.reactivex.functions.Consumer;

/**
 * Created by Abhishek on 12/3/2017.
 */

public class GetProductVariantUseCase {

    private final CategoryRepository categoryRepository;

    public GetProductVariantUseCase(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public void getVariants(Consumer<List<ProductVariant>> consumer, int categoryId) {
        categoryRepository.variants(consumer, categoryId);
    }
}