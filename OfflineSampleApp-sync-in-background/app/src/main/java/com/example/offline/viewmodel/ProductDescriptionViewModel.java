package com.example.offline.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.offline.domain.GetProductUseCase;
import com.example.offline.domain.GetProductVariantUseCase;
import com.example.offline.model.entity.ProductInfo;
import com.example.offline.model.entity.ProductVariant;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import timber.log.Timber;
/**
 * Created by Abhishek on 12/3/2017.
 */

public class ProductDescriptionViewModel extends ViewModel {


    private final GetProductUseCase getProductUseCase;
    private  final GetProductVariantUseCase getProductVariantUseCase;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private MutableLiveData<ProductInfo> productLiveData = new MutableLiveData<>();
    private MutableLiveData<List<ProductVariant>> productVariantLiveData = new MutableLiveData<>();

    private int categoryId;
    private int productId;
    public ProductDescriptionViewModel(GetProductUseCase getProductUseCase, GetProductVariantUseCase getProductVariantUseCase) {
        this.getProductUseCase = getProductUseCase;
        this.getProductVariantUseCase = getProductVariantUseCase;
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }


    public LiveData<ProductInfo> comments() {
        return productLiveData;
    }

    public LiveData<List<ProductVariant>> variants() {
        return productVariantLiveData;
    }

    void loadProducts() {
        getProductUseCase.getProductById(new ProductObserver(), categoryId, productId);
    }
    void loadVariants() {
        getProductVariantUseCase.getVariants(new VariantsObserver(), productId);
    }
    private final class ProductObserver implements Consumer<ProductInfo> {

        @Override
        public void accept(ProductInfo product) throws Exception {
            Timber.i("categoryInfo found","");
            productLiveData.setValue(product);
        }
    }

    private final class VariantsObserver implements Consumer<List<ProductVariant>> {

        @Override
        public void accept(List<ProductVariant> productInfoList) throws Exception {
            Timber.i("categoryInfo found","");
            productVariantLiveData.setValue(productInfoList);
        }
    }

    public void setIds(int categoryId, int productId) {
        this.categoryId = categoryId;
        this.productId = productId;
        loadProducts();
        loadVariants();
    }




}
