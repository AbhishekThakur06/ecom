package com.example.offline.data.exception;

/**
 * Exception throw by the application when a data search can't return a valid result.
 */
public class ProductDataNotFoundException extends Exception {
  public ProductDataNotFoundException() {
    super();
  }
}
