package com.example.offline.view.callback;

import com.example.offline.model.entity.CategoryInfo;

public interface CategoryClickCallback {
    void onClick(CategoryInfo project);
}
