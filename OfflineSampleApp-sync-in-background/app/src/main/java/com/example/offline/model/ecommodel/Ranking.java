
package com.example.offline.model.ecommodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Ranking {

    @SerializedName("ranking")
    @Expose
    private String ranking;
    @SerializedName("products")
    @Expose
    private List<RankedProduct> products = null;

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public List<RankedProduct> getProducts() {
        return products;
    }

    public void setProducts(List<RankedProduct> products) {
        this.products = products;
    }

}
