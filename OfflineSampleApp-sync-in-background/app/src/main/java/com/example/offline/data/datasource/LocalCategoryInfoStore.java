package com.example.offline.data.datasource;

import com.example.offline.model.entity.CategoryInfo;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 11/30/2017.
 */

public interface LocalCategoryInfoStore {
    void putCategories(List<CategoryInfo> categoryInfoList);
    Flowable<List<CategoryInfo>> getCategories();
    void clearTable();
}
