package com.example.offline.domain;

import com.example.offline.model.entity.CategoryInfo;

import java.util.List;

import io.reactivex.subscribers.ResourceSubscriber;

/**
 * Created by Abhishek on 11/30/2017.
 */

public class GetCategoriesUseCase {

    private final CategoryRepository categoryRepository;

    public GetCategoriesUseCase(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public void getCategories(ResourceSubscriber<List<CategoryInfo>> consumer) {
        categoryRepository.categories(consumer);
    }
}
