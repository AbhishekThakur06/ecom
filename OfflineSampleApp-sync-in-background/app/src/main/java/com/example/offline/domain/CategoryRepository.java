package com.example.offline.domain;

import com.example.offline.model.RankingProductResponse;
import com.example.offline.model.entity.CategoryInfo;
import com.example.offline.model.entity.ProductInfo;
import com.example.offline.model.entity.ProductVariant;

import java.util.List;

import io.reactivex.functions.Consumer;
import io.reactivex.subscribers.ResourceSubscriber;

/**
 * Created by Abhishek on 11/30/2017.
 */

public interface CategoryRepository {
    /**
     * emit a list of CategoryInfo and notify the consumer
     */
    void categories(ResourceSubscriber<List<CategoryInfo>> consumer);

    /**
     * emit a list of Products within a specific category id @param categoryId
     * and notify the consumer
     */
    void products(Consumer<List<ProductInfo>> consumer, int categoryId);

    /**
     * emit a Product associated with a specific category id @param categoryId
     * and  @param productId and notify the consumer
     */
    void product(Consumer<ProductInfo> consumer, int categoryId, int productId);

    /**
     * emit a List of ProductVariant associated with a specific
     *
     * @param productId and notify the consumer
     */
    void variants(Consumer<List<ProductVariant>> consumer, int productId);

    /**
     * emit a List of available Ranking type
     * and notify the consumer
     */
    void rankingName(Consumer<List<String>> consumer);

    /**
     * emit a List of Products fall under the give rank
     *
     * @param rank and notify the consumer
     */
    void getProductByRank(Consumer<List<RankingProductResponse>> consumer, String rank);
}
