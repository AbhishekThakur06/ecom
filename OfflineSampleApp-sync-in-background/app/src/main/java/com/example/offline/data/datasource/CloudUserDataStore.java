
package com.example.offline.data.datasource;

import com.example.offline.data.net.RestApi;
import com.example.offline.model.ecommodel.EcomData;

import io.reactivex.Observable;

/**
 * {@link RemoteDataStore} implementation based on connections to the api (Cloud).
 */
class CloudUserDataStore implements RemoteDataStore {

  private final RestApi restApi;
  /**
   * Construct a {@link RemoteDataStore} based on connections to the api (Cloud).
   *
   * @param restApi The {@link RestApi} implementation to use.
   *
   */
  CloudUserDataStore(RestApi restApi) {
    this.restApi = restApi;
  }

  @Override public Observable<EcomData> getProductData() {
    return this.restApi.ecomData();
  }

}
