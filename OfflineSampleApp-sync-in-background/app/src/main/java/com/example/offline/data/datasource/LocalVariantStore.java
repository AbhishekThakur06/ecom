package com.example.offline.data.datasource;

import com.example.offline.model.entity.ProductVariant;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 12/1/2017.
 */

public interface LocalVariantStore {

    void putVariants(List<ProductVariant> productVariants);
    Flowable<List<ProductVariant>> getProductVariants(int productId);
    void clearTable();
}
