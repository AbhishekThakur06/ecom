package com.example.offline.view.adapter;

/**
 * Created by Abhishek on 12/4/2017.
 */

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.offline.R;
import com.example.offline.model.RankingProductResponse;
import com.example.offline.view.callback.RankedProductClickCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by Abhishek on 12/3/2017.
 */

public class RankedProductListAdapter extends RecyclerView.Adapter<RankedProductListAdapter.ViewHolder> {

    private final List<RankingProductResponse> productInfoDataSet;
    private final RankedProductClickCallback productClickCallback;
    public RankedProductListAdapter(List<RankingProductResponse> productInfoDataSet, RankedProductClickCallback productClickCallback) {
        this.productInfoDataSet = productInfoDataSet;
        this.productClickCallback = productClickCallback;
    }

    @Override
    public RankedProductListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ranked_product_row, parent, false);
        return new RankedProductListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RankedProductListAdapter.ViewHolder holder, int position) {
        final RankingProductResponse productInfo = productInfoDataSet.get(position);
        holder.commentText.setTextColor(Color.BLACK);
        holder.commentText.setText(productInfo.getProductName());
        if(productInfo.getShares() > 0) {
            holder.share.setText("Share "+productInfo.getShares());
            holder.share.setVisibility(View.VISIBLE);
        } else {
            holder.share.setVisibility(View.GONE);
        }
        if(productInfo.getOrderCount() > 0) {
            holder.orders.setText("Orders "+productInfo.getOrderCount());
            holder.orders.setVisibility(View.VISIBLE);
        } else {
            holder.orders.setVisibility(View.GONE);
        }
        if(productInfo.getViewCount() > 0) {
            holder.viewed.setText("Viewed "+productInfo.getViewCount());
            holder.viewed.setVisibility(View.VISIBLE);
        } else {
            holder.viewed.setVisibility(View.GONE);
        }
        holder.commentText.setOnClickListener(new OnProductClickListener(productInfoDataSet.get(position)));
    }

    @Override
    public int getItemCount() {
        return productInfoDataSet == null ? 0 : productInfoDataSet.size();
    }

    public void updateCommentList(List<RankingProductResponse> products) {
        Timber.d("Got new product " + products.size());
        this.productInfoDataSet.clear();
        this.productInfoDataSet.addAll(products);
        notifyDataSetChanged();
    }

    /**
     * View holder for shopping list items of this adapter
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_name)
        TextView commentText;
        @BindView(R.id.share)
        TextView share;
        @BindView(R.id.view)
        TextView viewed;
        @BindView(R.id.orders)
        TextView orders;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    class OnProductClickListener implements View.OnClickListener {
        RankingProductResponse productInfo;
        OnProductClickListener(RankingProductResponse productInfo) {
            this.productInfo = productInfo;
        }
        @Override
        public void onClick(View view) {
            productClickCallback.onClick(productInfo);
        }
    };
}


