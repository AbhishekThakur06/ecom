package com.example.offline.view.fragment;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.offline.R;
import com.example.offline.di.Injectable;
import com.example.offline.model.DataWrapper;
import com.example.offline.model.entity.CategoryInfo;
import com.example.offline.view.MainActivity;
import com.example.offline.view.adapter.CategoryListAdapter;
import com.example.offline.view.callback.CategoryClickCallback;
import com.example.offline.viewmodel.AppViewModelFactory;
import com.example.offline.viewmodel.CategoryViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public class CategoryFragment extends LifecycleFragment implements Injectable {
    @Inject
    AppViewModelFactory viewModelFactory;
    @BindView(R.id.ranking_container)
    HorizontalScrollView horizontalScrollView;
    @BindView(R.id.ranking)
    LinearLayout rankingContainer;
    @BindView(R.id.comments_recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.progress)
    ProgressBar progress;
    private CategoryListAdapter recyclerViewAdapter;

    private CategoryViewModel viewModel;

    private LifecycleRegistry registry = new LifecycleRegistry(this);

    private LayoutInflater inflater;
    @Override
    public LifecycleRegistry getLifecycle() {
        return registry;
    }

    private final CompositeDisposable disposable = new CompositeDisposable();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        // Inflate this data binding layout
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, view);
        this.inflater = inflater;
        initRecyclerView();

        // Create and set the adapter for the RecyclerView.
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CategoryViewModel.class);
        progress.setVisibility(View.VISIBLE);
        viewModel.categories().observe(this, this::handleStreamOfCategory);
        viewModel.rankings().observe(this, this::updateRankings);

    }
    /** Creates project fragment for specific project ID */
    public static CategoryFragment forProject(String projectID) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        //args.putString(KEY_PROJECT_ID, projectID);
        fragment.setArguments(args);

        return fragment;
    }

    private void initRecyclerView() {
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager recyclerViewLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(recyclerViewLayoutManager);

        recyclerViewAdapter = new CategoryListAdapter(new ArrayList<>(), projectClickCallback );
        recyclerView.setAdapter(recyclerViewAdapter);
    }



    private final CategoryClickCallback projectClickCallback = new CategoryClickCallback() {
        @Override
        public void onClick(CategoryInfo info) {
            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                ((MainActivity) getActivity()).showProductFragment(info.getCategoryId(), null);
            }
        }
    };
    private void updateRankings(List<String> rankings) {
        Timber.i("Found rankings");
        rankingContainer.removeAllViews();
        for(int i = 0; i< rankings.size(); i++) {
            View v = inflater.inflate(R.layout.ranking_item, null, false);
            Button button = (Button) v.findViewById(R.id.rank);
            button.setText(rankings.get(i));
            button.setOnClickListener(new OnRankingClicked(rankings.get(i)));
            rankingContainer.addView(v);
        }
    }

    private void handleStreamOfCategory(DataWrapper<List<CategoryInfo>> dataWrapper) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progress.setVisibility(View.GONE);
                if(dataWrapper.getError() != null) {
                    Toast.makeText(getActivity(), dataWrapper.getError(), Toast.LENGTH_SHORT).show();
                } else {
                    recyclerViewAdapter.updateCommentList(dataWrapper.getData());
                    viewModel.refreshRankings();
                }
            }
        });

    }

    class OnRankingClicked implements View.OnClickListener {
        String rankId;
        OnRankingClicked(String rankId) {
            this.rankId = rankId;
        }
        @Override
        public void onClick(View view) {
            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                ((MainActivity) getActivity()).showProductFragment(-1,rankId);
            }
        }
    }
}
