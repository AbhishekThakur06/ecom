package com.example.offline.model.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;

/**
 * Created by Abhishek on 11/30/2017.
 */
@Entity
public class ProductRanking {

    @Expose
    @PrimaryKey(autoGenerate = true)
    private long id;

    @Expose
    @ColumnInfo(name = "product_id")
    private int productId;

    @Expose
    @ColumnInfo(name = "rank")
    private String rank;

    @Expose
    @ColumnInfo(name = "view_count")
    private int viewCount;


    @Expose
    @ColumnInfo(name = "order_count")
    private int orderCount;


    @Expose
    @ColumnInfo(name = "shares")
    private int shares;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }
}
