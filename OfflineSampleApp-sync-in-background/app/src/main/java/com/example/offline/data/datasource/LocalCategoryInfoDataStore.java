package com.example.offline.data.datasource;

import com.example.offline.data.datasource.dao.CategoryInfoDao;
import com.example.offline.model.entity.CategoryInfo;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 11/30/2017.
 */

public class LocalCategoryInfoDataStore implements LocalCategoryInfoStore {
    private final CategoryInfoDao categoryInfoDao;
    public LocalCategoryInfoDataStore(CategoryInfoDao categoryInfoDao) {
        this.categoryInfoDao = categoryInfoDao;
    }

    @Override
    public void putCategories(List<CategoryInfo> categoryInfoList) {

         categoryInfoDao.add(categoryInfoList) ;
    }

    @Override
    public Flowable<List<CategoryInfo>> getCategories() {

        return categoryInfoDao.getCategoryInfo();
    }

    @Override
    public void clearTable() {
        categoryInfoDao.clearTable(0);
    }
}
