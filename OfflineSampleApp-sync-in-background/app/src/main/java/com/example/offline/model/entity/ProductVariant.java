package com.example.offline.model.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;

/**
 * Created by Abhishek on 11/30/2017.
 */
@Entity
public class ProductVariant {

    @Expose
    @PrimaryKey(autoGenerate = true)
    private long id;

    @Expose
    @ColumnInfo(name = "variant_id")
    private int variantId;

    @Expose
    @ColumnInfo(name = "product_id")
    private int productId;

    @Expose
    @ColumnInfo(name = "color")
    private String color;

    @Expose
    @ColumnInfo(name = "size")
    private int size;

    @Expose
    @ColumnInfo(name = "price")
    private float price;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

       public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getVariantId() {
        return variantId;
    }

    public void setVariantId(int variantId) {
        this.variantId = variantId;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
