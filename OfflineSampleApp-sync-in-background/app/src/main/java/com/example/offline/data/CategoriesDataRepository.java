package com.example.offline.data;

import com.example.offline.data.datasource.AppDataStoreFactory;
import com.example.offline.domain.CategoryRepository;
import com.example.offline.model.RankingProductResponse;
import com.example.offline.model.ecommodel.Category;
import com.example.offline.model.ecommodel.EcomData;
import com.example.offline.model.ecommodel.Product;
import com.example.offline.model.ecommodel.Variant;
import com.example.offline.model.entity.CategoryInfo;
import com.example.offline.model.entity.ProductInfo;
import com.example.offline.model.entity.ProductRanking;
import com.example.offline.model.entity.ProductVariant;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.ResourceSubscriber;
import timber.log.Timber;

/**
 * Created by Abhishek on 11/30/2017.
 */

public class CategoriesDataRepository implements CategoryRepository {

    AppDataStoreFactory dataStoreFactory;
    private final CompositeDisposable disposables;
    @Inject
    CategoriesDataRepository(AppDataStoreFactory dataStoreFactory) {
        this.dataStoreFactory = dataStoreFactory;
        disposables = new CompositeDisposable();

    }

    @Override
    public void categories(ResourceSubscriber<List<CategoryInfo>> consumer) {
            dataStoreFactory.createCloudDataStore().getProductData()
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(new DisposableObserver<EcomData>() {
                        @Override
                        public void onNext(EcomData data) {
                            clearAllTable();
                            mapDataToDao(data);

                            disposables.add(dataStoreFactory.getLocalCategoryInfoStore().getCategories()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeWith(consumer));

                        }

                        @Override
                        public void onError(Throwable e) {
                            consumer.onError(e);
                            Timber.i("Exception in fetching data",e);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });

    }

    @Override
    public void products(Consumer<List<ProductInfo>> consumer, int categoryId) {
        disposables.add(dataStoreFactory.getLocalProductInfoStore().getProducts(categoryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer,
                        t -> Timber.e(t, "get products error")));

    }

    @Override
    public void product(Consumer<ProductInfo> consumer, int categoryId, int productId) {
        disposables.add(dataStoreFactory.getLocalProductInfoStore().getProduct(categoryId, productId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer,
                        t -> Timber.e(t, "get products error")));

    }

    @Override
    public void variants(Consumer<List<ProductVariant>> consumer, int productId) {
        disposables.add(dataStoreFactory.getLocalProductVariantStore().getProductVariants(productId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer,
                        t -> Timber.e(t, "get products error")));

    }

    @Override
    public void rankingName(Consumer<List<String>> consumer) {
        disposables.add(dataStoreFactory.getLocalRankingStore().getRankingName()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer,
                        t -> Timber.e(t, "get products error")));

    }

    @Override
    public void getProductByRank(Consumer<List<RankingProductResponse>> consumer, String rank) {
        disposables.add(dataStoreFactory.getLocalRankingStore().getProductByRanking(rank)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(consumer,
                                t-> Timber.e(t, "getProduct by rank error")));
    }


    private void mapDataToDao(EcomData data) {
        if (data != null) {
            int size = data.getCategories().size();
            List<CategoryInfo> categoryInfoList = new ArrayList<>();
            List<ProductInfo> productInfoList = new ArrayList<>();
            List<ProductVariant> productVariants = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                Category category = data.getCategories().get(i);
                CategoryInfo categoryInfo = new CategoryInfo();
                categoryInfo.setCategoryId(category.getId());
                categoryInfo.setCategoryName(category.getName());
                List<Product> products = category.getProducts();
                int productListSize = products.size();
                for (int j = 0; j < productListSize; j++) {
                    ProductInfo productInfo = new ProductInfo();
                    productInfo.setCategoryId(category.getId());
                    productInfo.setProductId(products.get(j).getId());
                    productInfo.setProductName(products.get(j).getName());
                    productInfo.setDateAdded(products.get(j).getDateAdded());
                    productInfo.setTaxName(products.get(j).getTax().getName());
                    productInfo.setTaxValue(products.get(j).getTax().getValue());
                    productInfoList.add(productInfo);
                    List<Variant> variants = products.get(j).getVariants();
                    int variantsSize = variants.size();
                    for (int k = 0; k < variantsSize; k++) {
                        ProductVariant productVariant = new ProductVariant();
                        productVariant.setProductId(products.get(j).getId());
                        productVariant.setColor(variants.get(k).getColor());
                        productVariant.setSize(variants.get(k).getSize());
                        productVariant.setVariantId(variants.get(k).getId());
                        productVariant.setPrice(variants.get(k).getPrice());
                        productVariants.add(productVariant);
                    }
                }
                categoryInfoList.add(categoryInfo);
            }
            dataStoreFactory.getLocalCategoryInfoStore()
                    .putCategories(categoryInfoList);
            dataStoreFactory.getLocalProductInfoStore()
                    .putProducts(productInfoList);
            dataStoreFactory.getLocalProductVariantStore()
                    .putVariants(productVariants);

            List<ProductRanking> productRankings = new ArrayList<>();
            int dataRankingSize = data.getRankings().size();
            for (int i = 0; i < dataRankingSize; i++) {
                int dataRankingProductSize =  data.getRankings().get(i).getProducts().size();
                for(int j=0; j<dataRankingProductSize; j++ ) {
                    ProductRanking productRanking = new ProductRanking();
                    productRanking.setRank(data.getRankings().get(i).getRanking());
                    productRanking.setProductId(data.getRankings().get(i).getProducts().get(j).getId());
                    productRanking.setViewCount(data.getRankings().get(i).getProducts().get(j).getViewCount());
                    productRanking.setShares(data.getRankings().get(i).getProducts().get(j).getShares());
                    productRanking.setOrderCount(data.getRankings().get(i).getProducts().get(j).getOrderCount());
                    productRankings.add(productRanking);
                }
            }
            dataStoreFactory.getLocalRankingStore().add(productRankings);
        }
    }

    private void clearAllTable() {
        dataStoreFactory.getLocalCategoryInfoStore()
                .clearTable();
        dataStoreFactory.getLocalProductInfoStore()
                .clearTable();
        dataStoreFactory.getLocalProductVariantStore()
                .clearTable();
        dataStoreFactory.getLocalRankingStore()
                .clearTable();
    }

}
