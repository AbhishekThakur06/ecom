package com.example.offline.view.callback;

import com.example.offline.model.entity.ProductInfo;

/**
 * Created by Abhishek on 12/3/2017.
 */

public interface ProductClickCallback {
    public void onClick(ProductInfo productInfo);
}
