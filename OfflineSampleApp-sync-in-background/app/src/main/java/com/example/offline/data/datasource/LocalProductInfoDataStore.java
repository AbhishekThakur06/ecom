package com.example.offline.data.datasource;

import com.example.offline.data.datasource.dao.ProductInfoDao;
import com.example.offline.model.entity.ProductInfo;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Abhishek on 12/1/2017.
 */

public class LocalProductInfoDataStore implements LocalProductInfoStore {
    private final ProductInfoDao productInfoDao;
    public LocalProductInfoDataStore(ProductInfoDao productInfoDao) {
        this.productInfoDao = productInfoDao;
    }

    @Override
    public void putProducts(List<ProductInfo> productInfoList) {
        productInfoDao.add(productInfoList);
    }

    @Override
    public Flowable<List<ProductInfo>> getProducts(int categoryId) {
        return productInfoDao.getProductInfoList(categoryId);
    }

    @Override
    public Flowable<ProductInfo> getProduct(int categoryId, int productId) {
        return productInfoDao.getProduct(categoryId, productId);
    }

    @Override
    public void clearTable() {
        productInfoDao.clearTable(0);
    }
}